# setup-home-nas

Sets up an NFS server that points to a few RAID 1 arrays on an Arch
Linux machine.

```bash
# Option-1 - Partition disks using fdisk
bash -x ./part-option-1.sh && bash -x setup.sh

# Option-2 - Partition disks using existing sfdisk dumps
bash -x ./part-option-2.sh && bash -x setup.sh
```

To set up the RAID array after an OS reinstall:
```bash
bash -x ./setup-after_os_reinstall.sh
```
