#
# setup-home-nas/setup.sh
#
# This scripts should be run on the NAS machine, in the setup-home-nas
# repo, as a regular user.
#

# Create RAID-0 arrays
sudo pacman --noconfirm -Sy mdadm
sudo mdadm --create --verbose --level=1 --metadata=1.2 --raid-devices=2 /dev/md/videos   /dev/sda1 /dev/sdb1
sudo mdadm --create --verbose --level=1 --metadata=1.2 --raid-devices=2 /dev/md/music    /dev/sda2 /dev/sdb2
sudo mdadm --create --verbose --level=1 --metadata=1.2 --raid-devices=2 /dev/md/pictures /dev/sda3 /dev/sdb3
sudo mdadm --create --verbose --level=1 --metadata=1.2 --raid-devices=2 /dev/md/other    /dev/sda4 /dev/sdb4
sudo mdadm --detail --scan | sudo tee -a /etc/mdadm.conf
sudo mdadm --assemble --scan

# Create filesystems on RAID arrays
sudo mkfs.ext4 /dev/md/videos
sudo mkfs.ext4 /dev/md/music
sudo mkfs.ext4 /dev/md/pictures
sudo mkfs.ext4 /dev/md/other

# Mount RAID filesystems
sudo mkdir -p /mnt/nas/{music,pictures,other,videos}
cat ./fstab | sudo tee -a /etc/fstab
sudo systemctl daemon-reload
sudo mount -a

# Install, enable, start the NFS server
sudo pacman --noconfirm -S nfs-utils
sudo cp ./exports /etc/exports
sudo systemctl enable nfs-server.service
sudo systemctl start nfs-server.service
