sudo mdadm --assemble /dev/md/videos --uuid 94fea162-8a3d-33db-e560-9086cd7ed59c
sudo mdadm --assemble /dev/md/music --uuid 274b8e55-002b-287a-64b6-2cf8b2344f11
sudo mdadm --assemble /dev/md/pictures --uuid 849db669-77ce-5f84-97ff-46a7ba844bd3
sudo mdadm --assemble /dev/md/other --uuid 3e82c6e6-3157-fecf-25c4-c9d2b855b0df

# Install, enable, start the NFS server
sudo pacman --noconfirm -S nfs-utils
sudo cp ./exports /etc/exports
sudo systemctl enable nfs-server.service
sudo systemctl start nfs-server.service
