# setup-home-nas/partition-option-1.sh
#
# This was executed to produce the sda.dump and sdb.dump. You shouldn't
# have to run this again if you have the .dump files.
#

for devName in sda sdb; do
  echo -e "Partitioning /dev/$devName"
  # Create partition table
  sudo parted -s /dev/$devName mklabel gpt
  # Create partitions
  sudo parted -s /dev/$devName unit mib -a optimal mkpart primary 0% 1048576MiB
  sudo parted -s /dev/$devName unit mib -a optimal mkpart primary 1048576MiB 1572864MiB
  sudo parted -s /dev/$devName unit mib -a optimal mkpart primary 1572864MiB 1835008MiB
  sudo parted -s /dev/$devName unit mib -a optimal mkpart primary 1835008MiB 1906688MiB
  # Store partition arrangement
  sudo sfdisk -d /dev/$devName > $devName.dump
  # Change partition type to Linux RAID
  # See Wikipedia article about partition type GUIDs
  # https://en.wikipedia.org/wiki/GUID_Partition_Table#Partition_type_GUIDs
  sed -i 's/type=0FC63DAF-8483-4772-8E79-3D69D8477DE4/type=A19D880F-05FC-4D3B-A006-743F0F84911E/' $devName.dump
  # Repartition with new partition type
  sudo sfdisk /dev/$devName $devName.dump
done
