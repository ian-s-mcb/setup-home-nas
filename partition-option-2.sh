#
# setup-home-nas/partition-option-2.sh
#
# Use .dump files to partition the disks for the RAID array.
#
# Partition NAS disks
sudo sfdisk /dev/sda < sda.dump
sudo sfdisk /dev/sdb < sdb.dump
